#!/bin/bash
# Man in Preview
# Created by Alexander Celeste, alex@tenseg.net
# March 2018
# 
# Opens man pages in Preview when not in SSH.

if [ -n "${SSH_CONNECTION}" ]
then
	/usr/bin/man $*;
else
	/usr/bin/man -t $* > "/tmp/man $*.ps"
	open -a /Applications/Preview.app/ "/tmp/man $*.ps"
fi