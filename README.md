# Man in Preview

* Created by Alexander Celeste, [alex@tenseg.net](mailto:alex@tenseg.net)
* March 2018

This script will open manpages in Preview if not accessed via SSH.

## Usage

Simply run the `man.sh` shell script followed by whatever command you want the manpage for, multiple args are supported.

You may want to `ln -s` the `man.sh` script to `man` somewhere that is in your shell's PATH to make it easier to run. Ensure that where it lives is higher in your PATH than the built-in commands. You will also need to `chmod a+x` the symlink to get it to run in the command line.

## Feedback

Any feedback is welcome. Feel free to email me or submit issues on the [Bitbucket repository](https://bitbucket.org/alexclst/man-in-preview).